#!/bin/sh

echo "Init backup sidecar"
# Generate config file from env_variable
echo "[default]
access_key = $S3_ACCESS_KEY
secret_key = $S3_SECRET_KEY
host_base = $S3_ENDPOINT
host_bucket = %(bucket)s.$S3_ENDPOINT
" > ~/.s3cfg

if [[ -z "${BKP_DB}" ]]; then
  db_file="db_dump_latest.sql"
else
  db_file="${BKP_DB}"
fi

if [[ -z "${BKP_FOLDER}" ]]; then
  htlm_folder="html_latest.tar.gz"
else
  htlm_folder="${BKP_FOLDER}"
fi

echo "Installing crons in /etc/periodic/"

touch /var/log/bakup.log

echo "Hello world !" >> /var/log/backup.log

echo "#!/bin/sh
echo \"Starting config backup script\" >> /var/log/backup.log

echo \"Compressing html folder\" >> /var/log/backup.log
cd /var/www
tar czf $htlm_folder --exclude={\"nextcloud.db\",\"*.log\"} html >> /var/log/backup.log

echo \"uploading $htlm_folder to s3://$S3_BUCKET/$PATH_TO_BACKUP$htlm_folder\" >> /var/log/backup.log

s3cmd put $htlm_folder s3://$S3_BUCKET/$PATH_TO_BACKUP$htlm_folder >> /var/log/backup.log

echo \"Config backup complete\" >> /var/log/backup.log
" > /etc/periodic/hourly/backup_config.sh



echo "#!/bin/sh
echo \"Starting db backup script\" >> /var/log/backup.log

sqlite3 /var/www/html/data/nextcloud.db \".backup $db_file\"

echo \"DB snapshoted\" >> /var/log/backup.log

s3cmd put $db_file s3://$S3_BUCKET/$PATH_TO_BACKUP$db_file >> /var/log/backup.log

echo \"DB backup complete\" >> /var/log/backup.log
" > /etc/periodic/15min/backup_db.sh

chmod +x /etc/periodic/hourly/backup_config.sh
chmod +x /etc/periodic/15min/backup_db.sh

echo "Installation done"

crond && tail -f "/var/log/backup.log"
