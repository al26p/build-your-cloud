#!/bin/sh

echo "Init restoration container"
# Generate config file from env_variable
echo "[default]
access_key = $S3_ACCESS_KEY
secret_key = $S3_SECRET_KEY
host_base = $S3_ENDPOINT
host_bucket = %(bucket)s.$S3_ENDPOINT
" > ~/.s3cfg


# TODO: Choose the backup
# sqlite3 $db_location '.backup /app/db_dump_$(date +"%Y-%m-%d_%H:%M:%SZ").sqlite'
if [[ -z "${BKP_DB}" ]]; then
  db_file="db_dump_latest.sql"
else
  db_file="${BKP_DB}"
fi

if [[ -z "${BKP_FOLDER}" ]]; then
  html_folder="html_latest.tar.gz"
else
  html_folder="${BKP_FOLDER}"
fi

mkdir -p /var/www/html/data

echo "Trying to retore s3://$S3_BUCKET/$PATH_TO_BACKUP$html_folder to /var/www/html_latest.tar.gz"

s3cmd get s3://$S3_BUCKET/$PATH_TO_BACKUP$html_folder /var/www/html_latest.tar.gz

echo "Trying to retore s3://$S3_BUCKET/$PATH_TO_BACKUP$db_file to /var/www/html/data/nextcloud.db"

s3cmd get --force s3://$S3_BUCKET/$PATH_TO_BACKUP$db_file /var/www/html/data/nextcloud.db

echo "Starting custom restauration script"

echo \"Extracting html folder\"
cd /var/www/
tar xzf html_latest.tar.gz 
echo \"Cleaning up archive\"
rm html_latest.tar.gz 

echo "Restoration complete"

chown 33:33 -R /var/www/

echo "Done"
