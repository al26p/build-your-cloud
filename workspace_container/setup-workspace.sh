#!/bin/sh

apk add curl nano vim

#Installing S3 utilities
pip install s3cmd

#Installing kubectl
curl -L "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" -o /usr/bin/kubectl
chmod +x /usr/bin/kubectl
echo "
# Customer script
alias k=kubectl
alias s3=s3cmd" >> /etc/profile

#Installing k9s
cd /usr/bin/
curl -L https://github.com/derailed/k9s/releases/download/v0.27.4/k9s_Linux_amd64.tar.gz | tar -xz k9s